��    H      \  a   �         .   !     P     W     `     l          �     �     �     �     �     �     �     	          !  
   5     @     P     ^     r     �     �     �     �     �     �     �               '     5     B     R     d     r     �  	   �     �  
   �  
   �     �  
   �     �     	     	     ?	     Y	     q	  /   �	     �	     �	     �	     �	     
     
     %
     -
     @
     S
     d
     p
     �
     �
     �
     �
     �
     �
     �
     �
  !     (   1  "   Z  
   }     �     �     �  �   �     �     �  
   �     �     �     �     �     �     �  -        F     X     i     y     �  &   �  (   �     �  D        U     m     �  !   �  #   �  "   �     
     !     8     H  3   ]     �     �     �     �  L   �  4   *     _  	   e     o  !   ~     �     �     �     �     �       8     ,   A  
   n     y     �      �      �     �     �          $     ?     Z     y     �     �  2   �  >   �  �        :                             $   D                 )          B   ?   1                         2      G   E   ,          /   7          <   0      =      *   6          4   5       	         (   +   9   !   
                    >       -      8   .                A   @   ;             &   H          "   3   F           #      %                 '   C    Voller Zugriff auf den Administrationsbereich. avatar benutzer benutzer2FA benutzerBearbeiten benutzerDesc benutzerGueltigBis benutzerKategorie benutzerLetzterLogin benutzerLoeschen benutzerLoginVersuche benutzerNeu benutzerSperren benutzerTab benutzerURL clickHereToCreateQR codeCreate codeCreateAgain emergencyCode emergencyCodeCreate entsperrenLabel errorAtLeastOneAdmin errorAtLeastOneRight errorGroupDelete errorGroupDeleteCustomer errorImageMissing errorImageUpload errorKeyChange errorLockAdmin errorSelfDelete errorSelfLock errorUserAdd errorUserDelete errorUserNotFound errorUserSave errorUsernameMissing gruppeBearbeiten gruppeNeu gruppenKategorie gruppenTab infoScanQR loeschenLabelDeaktiviert noMenuItem permission_DASHBOARD_VIEW permission_DBCHECK_VIEW permission_EXPORT_SHOPINFO_VIEW permission_FILECHECK_VIEW permission_IMAGE_UPLOAD permission_PERMISSIONCHECK_VIEW permission_SETTINGS_META_KEYWORD_BLACKLIST_VIEW permission_SETTINGS_SEARCH_VIEW personalInformation resume shopEmergencyCodes sperrenLabel stateOFF stateON successGroupCreate successGroupDelete successGroupEdit successLock successUnlocked successUserAdd successUserDelete successUserSave sureDeleteGroup temporaryAccess tillInclusive usernameNotAvailable warningAuthSecretOverwrite warningNoPermissionToBackendAfter Content-Type: text/plain; charset=UTF-8
 Full access to administration area User image Users and rights Two-factor authentication Editing a user Here you manage the user and group rights for the backend of JTL-Shop. You can add new users and assign them to a group. In the tab "User groups" you can set the permissions for specific areas of the back end. Valid until Managing users Last login Delete user Failed login attempts Creating a user Block access Users https://jtl-url.de/tw0dn To generate a new QR code, please click here: Generate new code Create new codes Emergency codes Generate new emergency codes Unblock user At least one administrator must exist. You must select at least one permission. Could not delete user group. The user group cannot be deleted because it has at least one member. Please select an image. Error uploading the image.  %s could not be edited! Administrators cannot be blocked. You cannot delete your own account. You cannot block your own account. Could not create user. Could not delete user. User not found. Could not save user. Please specify a user name in the login data first. Editing a user group Creating a user group Managing user groups User groups Scan the displayed QR code using the Google Authenticator App on your phone. Cannot be deleted. The user group still has members. Other Dashboard Check database Export file shopinfo.xml (elm@ar) Check online shop files Image upload Check directory Blacklist for meta keywords Search settings Author About the author (for example displayed with blog posts) Emergency codes for the back end of JTL-Shop Block user Disabled Enabled User group created successfully. User group deleted successfully. User group edited successfully. User blocked successfully. User unblocked successfully. User created successfully. User deleted successfully. User data edited successfully. Delete group? Temporary access Up to and including User name <strong>%s</strong> is already assigned. The former "Authentication Secret" will be replaced. Continue? Please note that after saving, you can no longer access the backend of JTL-Shop with this user account if you do not have access to the Google Authenticator App via a mobile device. 